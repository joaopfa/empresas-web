import styled from 'styled-components';
import {theme} from "../../Theme";

export const ContainerLogin = styled.div `
    height: fit-content;
    width: 400px;
    margin: auto;
    display: flex;
    flex-direction: column;
    align-items: center;
    img{
        display: block;
        margin: auto;
    }
`
export const MainTxt = styled.h2 `
    color: #383743;
    width: 178.9px;
    text-align: center;
    font-size: 25px;
    margin: 70px auto 24px;
`
export const SecondTxt = styled.p `
    color: #383743;
    width: 280px;
    text-align: center;
    font-size: 16px;
    margin: 12.5px auto 30px;
`
export const InputDiv = styled.div `
    border-width: 0 0 2px 0;
    border-style: solid;
    border-color: #383743;
    width: 350px;
    margin-top: 30px;
    display: flex;
        img {
            margin: unset;
        }
        i {
            color: #383743;
            cursor: pointer;
            font-size: 24px;
            margin-left: 5%;
       }

`
export const NkdInput = styled.input `
    width: 70%;
    font-size: 20px;
    display: block;
    margin-bottom: 1px;
    margin-left: 20px;
    border-style: none;
    outline: none;
    background-color: transparent;
    -webkit-autofill,
    :-webkit-autofill:hover, 
    :-webkit-autofill:focus, 
    :-webkit-autofill:active  {
    -webkit-box-shadow: 0 0 0 30px  ${theme.beige} inset !important;
    }
    ::placeholder,
    ::-webkit-input-placeholder {
    font-size: 1.3rem;
     }

`
export const Button = styled.button `
    display: block;
    margin: 50px auto;
    width: 350px;
    font-size: 20px;
    font-weight: 500;
    color: ${theme.whiteTwo};
    border-style: none;
    background-color: ${theme.greenyBlue};
    height: 50px;
    border-radius:2px;
    :hover{
        cursor: pointer;
        background-color: ${theme.warmGrey};
    }
    :active {
        outline: none;
        background-color: ${theme.darkIndigo};
    }

`
