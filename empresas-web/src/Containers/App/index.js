import React from 'react';
import Router from "../Router";
import { createBrowserHistory } from "history";
import { Provider } from "react-redux";
import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import { generateReducers } from "../../Reducers";
import { routerMiddleware } from "connected-react-router";
import {GlobalStyle} from "../../Theme";

export const history = createBrowserHistory();

const middlewares = [
  applyMiddleware(routerMiddleware(history), thunk),
  window.__REDUX_DEVTOOLS_EXTENSION__
    ? window.__REDUX_DEVTOOLS_EXTENSION__()
    : f => f
];

const store = createStore(generateReducers(history), compose(...middlewares));

export const App = () => (
    <Provider store={store}>
      <GlobalStyle/>  
      <Router history={history} />
    </Provider>
  );

export default App;

