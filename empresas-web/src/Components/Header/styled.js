import styled from 'styled-components'
import {theme} from "../../Theme";
import {InputDiv, NkdInput} from "../../Containers/Login/styled"

export const StyledHeader = styled.header `
    width: 100%;
    height: 100px;
    background-image: linear-gradient(540deg,#ee4c77 6%,#0d0430 250%);
    display: flex;
    img {
        align-self: center;
        margin: auto;
    }
    i {
        color: white;
        font-size: 30px;
        position: absolute;
        align-self: center;
        right: 30px;
        cursor: pointer;
    }
`
export const SearchDiv = styled(InputDiv) `
    margin: auto;
    align-self: center;
    width: 80%;
    height: 50%;
    border-width: 0 0 1px 0;
    border-color: ${theme.white};
`
export const SearchInput = styled(NkdInput) `
    width: 100%;
    caret-color: ${theme.white};
    ::placeholder,
    ::-webkit-input-placeholder {
    color: #991237;
     }
`