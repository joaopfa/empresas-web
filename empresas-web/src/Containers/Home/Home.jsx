import React from 'react';
import Header from '../../Components/Header';
import {BodyComp} from '../../Components/Body/styled';
import { push } from "connected-react-router";
import { routes } from '../Router';
import { connect } from "react-redux";
import {MainTxt} from '../Login/styled';

class Home extends React.Component{
  constructor(props) {
    super(props)
    this.state = {
      serach: ''
    };
  };

  render() {
    return (
          <BodyComp>            
              <Header/>
              <MainTxt>
                Clique na busca para iniciar
              </MainTxt>
          </BodyComp>        
        
    );
  }
}
  
export default Home 