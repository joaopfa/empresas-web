import React from 'react';
import {StyledHeader, SearchInput, SearchDiv} from './styled';
import NavLogo from '../../Assets/logo/logo-nav.png'

function Header() {
    const [hideLogo, setLogoDisplay] = React.useState(false)
    const toogleNavDisplay = () => {
        setLogoDisplay(!hideLogo)
    }
       return (
            <StyledHeader >
                {hideLogo ? <SearchDiv><SearchInput placeholder="Pesquisar"/></SearchDiv> : <img src={NavLogo} alt="Ioasys"/>}
                
                <i onClick={toogleNavDisplay} class="fa fa-search"></i>
            </StyledHeader>
        );
    
}

export default Header;