import React from 'react';
import {ContainerLogin, MainTxt, SecondTxt, InputDiv, NkdInput, Button } from './styled';
import {BodyComp} from '../../Components/Body/styled';
import LogoIo from '../../Assets/logo/logo-home.png';
import EIcon from '../../Assets/Icons/Email/ic-email.png';
import LIcon from '../../Assets/Icons/Lock/ic-cadeado.png';
import { push } from "connected-react-router";
import { routes } from '../Router';
import { connect } from "react-redux";

class Login extends React.Component{
  constructor(props) {
    super(props)
    this.state = {
      isPasswordShown: false,
      email: '',
      password: ''
    };
  };

  togglePasswordVisibility = () => {
    const {isPasswordShown} = this.state;
    this.setState({ isPasswordShown: !isPasswordShown });
  }
  handleLogin = (event) => {
    event.preventDefault()
    this.props.loginUser(this.state.email, this.state.password)
  };
  handleFieldChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value
    })
  }

  render() {
    const { email, password, isPasswordShown } = this.state
    return (
        <BodyComp>            
            <ContainerLogin>
                <img alt="Foto perfil João Pedro" src={LogoIo}/>
                <MainTxt>BEM-VINDO AO EMPRESAS</MainTxt>
                <SecondTxt>
                Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.
                </SecondTxt>
                <form onSubmit={this.handleLogin}>          
                <InputDiv><img alt="E-mail" src={EIcon}/>
                    <NkdInput
                    placeholder="E-mail"
                    name="email"
                    required
                    type="text"
                    id="email"
                    onChange={this.handleFieldChange}
                    value={email} 
                    />
                </InputDiv>
                <InputDiv><img alt="E-mail" src={LIcon}/>
                    <NkdInput
                    placeholder="Senha"
                    name="password"
                    required
                    type={(isPasswordShown)?"text" : "password" }
                    id="senha"           
                    onChange={this.handleFieldChange}
                    value={password} 
                    />
                    <i onClick={this.togglePasswordVisibility} 
                    className={`fa ${isPasswordShown ? "fa-eye-slash" : "fa-eye"} `}>
                    </i> {/*fazer renderizar apenas quando o input estiver ativo */}            
                </InputDiv>
                <Button type="submit">
                    ENTRAR
                </Button>
                </form>
            </ContainerLogin>
        </BodyComp>
    );
  }
}

const mapDispatchToProps = dispatch => ({ 
    loginUser: () => dispatch(push(routes.home)),
  })
  
export default connect (
    null, 
    mapDispatchToProps
  ) (Login);
