import { createGlobalStyle } from "styled-components";

export const GlobalStyle = createGlobalStyle`
  body {
      margin: 0;
      padding:0;
      -webkit-font-smoothing: antialiased;
      -moz-osx-font-smoothing: grayscale;
      font-family: 'Roboto', sans-serif;
      overflow-y: overlay;
      width: 100%; 
      padding-left: 0px !important;
    }
    
    * {
        box-sizing: border-box;
        font-family: 'Roboto', sans-serif;
    }
    @font-face {
    font-family: Roboto;
    src: local(Roboto-Regular),
        url(/path/to/Roboto-Regular.woff2) format("woff2"),
        url(/path/to/Roboto-Regular.woff) format("woff"),
        url(/path/to/Roboto-Regular.ttf) format("truetype");
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
  }
    
`;

export const theme = {
    white: "#d8d8d8",
    whiteTwo: "#ffffff",
    darkIndigo: "#1a0e49",
    warmGrey: "#8d8c8c",
    greenyBlue: "#57bbbc",
    beige: "#ebe9d7",
}