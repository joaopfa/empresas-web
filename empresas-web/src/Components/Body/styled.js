import styled from 'styled-components'
import {theme} from "../../Theme";

export const BodyComp = styled.div `
  width: 100vw;
  min-width: fit-content;
  background-color:${theme.beige};
  height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
`